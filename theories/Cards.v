Require Import Coq.ZArith.ZArith.
From Coq Require Import Lia.

(** * Machine-friendly algorithms to process tarot cards

Formalizing what the 78 cards of tarot are is a difficult task. What
we can say for sure is, among these 78 cards, is the excuse. This card
does not really have a suit. 56 of the cards have suits you are used
to, such as hearts, clubs, diamonds and spades. The 21 other cards are
the trumps, which could be considered its own fifth suit.  To
differentiate, we name the 4 other suits the “minor suits”.

Within a minor suit, you have 4 face cards (jack, knight, queen, king)
and 10 cards numbered from 1 to 10.  The trumps consist of 21 cards,
numbered from 1 to 21.

The first and last trump, as well as the excuse, have a special role,
they are called oudlers.

You could try and model this knowledge with inductive types, but it
would soon become cumbersome, and it would not be very
machine-friendly.  The approach here is to consider as the set of
cards, the set of the 78 integers from 0 to 77 (each fits neatly in 1
byte) and derive predicates.

*)

(** This algorithm checks that the card [c] is a valid card index,
i.e. it is less than 78. *)
Definition is_valid_card (c : Z) := (0 <= c < 78)%Z.

(** The suits themselves constitute a heterogeneous set: the minor
suits and the trumps are very different.  Instead of using another
inductive type for a suit, we just index them from 0 to 4.  Suit 0
shall be hearts, suit 1 shall be clubs, suit 2 shall be diamonds, suit
3 shall be spades, and suit 4 shall be the trumps.
*)

(** This algorithm checks that the number [s] identifies a suit,
i.e. it is less than 5. *)
Definition is_valid_suit (s : Z) := (0 <= s < 5)%Z.

(** More precisely, this algorithm checks if [s] identifies the trumps
suit. *)
Definition is_trump_suit (s : Z) := (s = 4)%Z.

(** In retrospect, this algorithm checks if [s] identifies any minor
suit. *)
Definition is_minor_suit (s : Z) := (0 <= s < 4)%Z.

(** In order to indentify card numbers, we index them with the
following scheme. The 21 trumps are indexed from 0 to 20 inclusive,
and the 14 members of each minor suit are indexed by their order:
first, the 10 non-face cards, and then the 4 faces. *)

(** This algorithm checks if [n] identifies a card index of a minor
suit, i.e. if it is positive and less than 14. *)
Definition is_valid_minor_number (n : Z) := (0 <= n < 14)%Z.

(** This algorithm checks if [n] identifies a trump, i.e. if it is
less than 21. *)
Definition is_valid_trump_number (n : Z) := (0 <= n < 21)%Z.

(** With this distinction, it is easy to know whether a card is of a
minor suit or if it is a trump: compare it to the card index 56. *)

(** This algorithm checks whether [c] is the index of a card of the
minor suit, i.e. less than 56. *)
Definition is_valid_minor_card (c : Z) := (0 <= c < 56)%Z.

(** This algorithm checks whether [c] is the index of a trump, i.e. at
least 56 and less than 77. *)
Definition is_valid_trump (c : Z) := (56 <= c < 77)%Z.

(** Now, how do you get a card index of a card of a minor suit or a
trump?  It turns out, this encoding scheme is well suited for that. *)

(** Return the card index of any minor card or trump: [s * 14 + n]. *)
Definition compose (n : Z) (s : Z) := (s * 14 + n)%Z.

(** Unfortunately, doing the reverse operation — getting a suit index
and a number index from a card index — is not as simple, because of
the suit imbalance.  There needs to be 2 cases: one if the card is a
trump, and one otherwise. *)

(** Return the suit of a card.  If [c <? 56], then return [c / 14],
and otherwise 4. *)
Definition get_suit (c : Z) : Z :=
  if (c <? 56)%Z
  then c / 14
  else 4.

(** Return the number of a card.  If [c <? 56], then return [c mod
14], and otherwise [c - 56]. *)
Definition get_number (c : Z) :=
  if (c <? 56)%Z
  then (c mod 14)%Z
  else (c - 56)%Z.

(** Now, do this card encoding scheme works? *)

(** This lemma tells us that if you take a minor card number and a
minor suit, you will get a valid minor card. *)
Lemma compose_minor : forall (n : Z) (s : Z),
    is_valid_minor_number n -> is_minor_suit s
    -> is_valid_minor_card (compose n s).
Proof.
  (** First, let’s pose our variables. *)
  intros n s number_valid suit_valid.
  (** If the number is valid, then we can extract an inequality. *)
  unfold is_valid_minor_number in number_valid.
  (** If the suit is valid, then we can extract another inequality. *)
  unfold is_minor_suit in suit_valid.
  (** Now, the card composition is a simple arithmetic formula: s * 14 + n. *)
  unfold compose.
  (** Checking that it is a valid minor card is simply checking that it
  is less than 56. *)
  unfold is_valid_minor_card.
  lia.
Qed.

(** This lemma tells us that if you take a trump number and the trump
suit, composition gives you a valid trump card. *)
Lemma compose_trump : forall (n : Z) (s : Z),
    is_valid_trump_number n -> is_trump_suit s
    -> is_valid_trump (compose n s).
Proof.
  (** Our variables are n, s, the fact that n is a valid trump number,
  and that s identifies the trump suit. *)
  intros n s number_valid suit_valid.
  (** We can be a little more specific about what constitutes a valid
  trump number (less than 21) and the trump suit (precisely 4). *)
  unfold is_valid_trump_number in number_valid.
  unfold is_trump_suit in suit_valid.
  (** Composition is a very simple arithmetic formula. *)
  unfold compose.
  (** Proving that a card is a valid trump is 2 inequalities (between
  56 and 77). *)
  unfold is_valid_trump.
  (** Both can easily be solved by the lia tactic. *)
  lia.
Qed.

(** To decompose a card is a little bit more difficult, because we
have to choose a code branch. *)

(** This lemma tells us that if we are looking at a minor card, it
cannot flow through the “trumps” branch of the decomposition
algorithm. *)
Lemma cannot_follow_false_branch :
  forall (c : Z),
    (c < 56)%Z -> ((c <? 56)%Z = false) -> False.
Proof.
  intros c c_lt_56 follow_branch_false.
  destruct (Z.ltb_nlt c 56%Z) as (direct, _).
  exact (direct follow_branch_false c_lt_56).
Qed.

(** This is similar, for trumps. *)
Lemma cannot_follow_true_branch :
  forall (c : Z),
    (c >= 56)%Z -> ((c <? 56)%Z = true) -> False.
Proof.
  intros c c_gt_56 follow_branch_true.
  destruct (Z.ltb_lt c 56) as (direct, _).
  cut (~(c < 56)%Z). intros c_nlt_56.
  exact (c_nlt_56 (direct follow_branch_true)).
  lia.
Qed.

(** This lemma tells us that it is always possible to split cases
where c is a trump or a minor card. *)
Lemma can_split_at_56 :
  forall (c : Z),
    (c >= 56)%Z \/ (c < 56)%Z.
Proof.
  intros c.
  lia.
Qed.

(** This lemma tells us that 14 is not 0. *)
Lemma zero_lt_14 : (0 < 14)%Z.
Proof.
  lia.
Qed.

Lemma zero_neq_14 : (14 <> 0)%Z.
Proof.
  lia.
Qed.

(** This lemma tells us that if we try and decompose a valid minor
card, we get a valid minor suit and a valid minor suit number. *)
Lemma decompose_minor_correct :
  forall (c : Z),
    is_valid_minor_card c
    -> (is_valid_minor_number (get_number c)
        /\ is_minor_suit (get_suit c)).
Proof.
  intros c (c_positive, c_lt_56).
  (** So we know c < 56, and we want to prove that decomposing it
  gives valid suits and numbers. Let’s start with a valid number.*)
  split.
  (** Apply the definitions. *)
  unfold get_number. unfold is_valid_minor_number.
  (** Examine both branches of the [get_number] algorithm. *)
  case_eq (c <? 56)%Z. intros follow_true_branch.
  (** The first branch is if [c < 56] (the interesting case. We are
  asked to prove that [c mod 14 < 14].*)
  exact (Z.mod_pos_bound c 14%Z zero_lt_14).
  (** We are now getting through the second branch of the algorithm,
  which is impossible. *)
  intros follow_false_branch.
  contradiction (cannot_follow_false_branch c c_lt_56 follow_false_branch).
  (** And now, let’s prove that the suit is a minor suit. *)
  unfold get_suit. unfold is_minor_suit.
  case_eq (c <? 56)%Z. intros follow_true_branch.
  (** We need to prove that [0 <= c / 14 < 4], given [0 <= c < 56]. We have a
  theorem to help us compare divisions with the same denominator, but
  it needs to be done with non-strict inequalities.  So first, let’s
  prove things with non-strict inequalities. *)
  cut ((0 <= c / 14 <= 3)%Z). intros non_strict_inequality.
  (** Let’s just assume that we get a suit index at most 3. Can we
  prove it is less than 4? *)
  lia.
  (** Now let’s prove it is at most 3. *)
  cut ((0 <= c <= 55)%Z). intros (_, c_le_55).
  (** If we know that [c <= 55], can we prove that [c / 14 <= 3]? *)
  split.
  exact (Z.div_le_mono 0 c 14 zero_lt_14 c_positive).
  exact (Z.div_le_mono c 55 14 zero_lt_14 c_le_55).
  (** Now let’s prove that [c <= 55] (we know that [c < 56]) *)
  lia.
  (** And now, we are following the false branch. *)
  intros follow_false_branch.
  contradiction (cannot_follow_false_branch c c_lt_56 follow_false_branch).
Qed.

(** This lemma proves that if we have a valid trump card, then
decomposing it will give us the trump suit and a valid trump
number. *)
Lemma decompose_trump_correct :
  forall (c : Z),
    is_valid_trump c
    -> (is_valid_trump_number (get_number c)
        /\ is_trump_suit (get_suit c)).
Proof.
  intros c (c_ge_56, c_lt_77).
  split.
  (** Let’s prove that it is a valid trump number first. *)
  unfold get_number. unfold is_valid_trump_number.
  (** Now, the result should be less than 21.  In the same way, we will
  show that even if we followed the true branch (for a minor card), it
  would still be the case. *)
  case_eq (c <? 56)%Z. intros follow_true_branch.
  contradiction (cannot_follow_true_branch c (Z.le_ge 56 c c_ge_56) follow_true_branch).
  intros _.
  (** Let’s prove the other branch, [c - 56 < 21], if [c < 77]. *)
  lia.
  (** Now, we just have to prove that the suit is equal to 4. *)
  unfold get_suit. unfold is_trump_suit.
  case_eq (c <? 56)%Z. intros follow_true_branch.
  contradiction (cannot_follow_true_branch c (Z.le_ge 56 c c_ge_56) follow_true_branch).
  intros _.
  reflexivity.
Qed.

(** There are only 2 supported ways to construct a card: from a valid
minor card composition, or from a valid trump card composition. *)
Definition is_valid_composition (n s : Z) :=
  ((is_valid_minor_number n /\ is_minor_suit s)
   \/ (is_valid_trump_number n /\ is_trump_suit s)).

(** This lemma tells us that if we find 2 ways to build a composition,
one way being with a trump composition, then the second way is also a
trump composition. *)
Lemma composition_is_injective_aux_aux :
  forall (n1 n2 s1 s2 : Z),
    is_valid_trump_number n1 ->
    is_trump_suit s1 ->
    is_valid_composition n2 s2 ->
    (compose n1 s1 = compose n2 s2) ->
    (is_valid_trump_number n2 /\ is_trump_suit s2).
Proof.
  intros n1 n2 s1 s2 n1_trump s1_trump c2_valid same_output.
  destruct c2_valid as [(n2_minor, s2_minor) | (n2_trumps, s2_trumps)].
  (** First case: c1 is trumps and c2 is minor. Contradiction, because
  they have the same output. *)
  unfold is_valid_trump_number in n1_trump.
  unfold is_trump_suit in s1_trump.
  unfold is_valid_minor_number in n2_minor.
  unfold is_minor_suit in s2_minor.
  unfold compose in same_output.
  (** The lia tactic should be able to prove that [c < 56] (from the
  c2 constraints) and [~ (c < 56)] (from the c1 constraints). *)
  cut ((0 <= s1 * 14 + n1 < 56)%Z). intros c_lt_56.
  cut (~ (0 <= s1 * 14 + n1 < 56)%Z). intros c_nlt_56.
  contradiction (c_nlt_56 c_lt_56).
  lia. lia.
  (** Second case: both are trumps. *)
  split. exact n2_trumps. exact s2_trumps.
Qed.

(** If we find 2 ways to compose the same card though, both
compositions are a minor composition or both are trumps compostions. *)
Lemma composition_is_injective_aux :
  forall (n1 n2 s1 s2 : Z),
    is_valid_composition n1 s1 ->
    is_valid_composition n2 s2 ->
    (compose n1 s1 = compose n2 s2) ->
    ((is_valid_minor_number n1 /\ is_minor_suit s1
      /\ is_valid_minor_number n2 /\ is_minor_suit s2)
     \/ (is_valid_trump_number n1 /\ is_trump_suit s1
         /\ is_valid_trump_number n2 /\ is_trump_suit s2)).
Proof.
intros n1 n2 s1 s2 c1_valid c2_valid same_output.
destruct c1_valid as [(n1_minor, s1_minor) | (n1_trump, s1_trump)].
destruct c2_valid as [(n2_minor, s2_minor) | (n2_trump, s2_trump)].
(** If both compositions are minor, prove the first option. *)
left.
split. exact n1_minor. split. exact s1_minor. split. exact n2_minor. exact s2_minor.
(** Now, the first composition is a minor suit, and the second is a
trump.  It is a contradiction that we get the same output. *)
cut (is_valid_composition n1 s1). intros c1_valid.
destruct (composition_is_injective_aux_aux n2 n1 s2 s1 n2_trump s2_trump c1_valid (eq_sym same_output)) as (n1_trump, s1_trump).
unfold is_minor_suit in s1_minor. unfold is_trump_suit in s1_trump.
cut (~ (0 <= s1 < 4)%Z). intros s1_nlt_4. contradiction (s1_nlt_4 s1_minor). lia.
(** We must remember that [n1] / [s1] is a valid composition. *)
unfold is_valid_composition. left. split. exact n1_minor. exact s1_minor.
(** And now, the case where [c1] is trumps. *)
destruct (composition_is_injective_aux_aux n1 n2 s1 s2 n1_trump s1_trump c2_valid same_output) as (n2_trump, s2_trump).
(** Prove the right-hand side: both are trumps. *)
right. split. exact n1_trump. split. exact s1_trump. split. exact n2_trump. exact s2_trump.
Qed.

(** This lemma says that the minor cards of a suit does not overflow
the suit boundaries. *)
Lemma minor_number_divided_by_14_zero :
  forall (n : Z),
    is_valid_minor_number n ->
    (n / 14 = 0)%Z.
Proof.
  intros n n_minor.
  unfold is_valid_minor_number in n_minor.
  exact (Z.div_small n 14 n_minor).
Qed.

(** This lemma shows that on the minor cards, the composition is injective. *)
Lemma minor_composition_is_injective :
  forall (n1 n2 s1 s2 : Z),
    is_valid_minor_number n1 ->
    is_minor_suit s1 ->
    is_valid_minor_number n2 ->
    is_minor_suit s2 ->
    (compose n1 s1 = compose n2 s2) ->
    (n1 = n2 /\ s1 = s2).
Proof.
  intros n1 n2 s1 s2 n1_minor _ n2_minor _ same_output.
  unfold compose in same_output.
  (** Start by proving that s1 = s2. This is because n1 < 14 and n2 <
  14, so they should disappear if we divide by 14. *)
  pose (n1_quotient_zero := minor_number_divided_by_14_zero n1 n1_minor).
  pose (n2_quotient_zero := minor_number_divided_by_14_zero n2 n2_minor).
  pose (c1_divided_by_14 := Z.div_add_l s1 14 n1 zero_neq_14).
  pose (c2_divided_by_14 := Z.div_add_l s2 14 n2 zero_neq_14).
  rewrite same_output in c1_divided_by_14.
  rewrite n2_quotient_zero in c2_divided_by_14.
  rewrite (Z.add_0_r s2) in c2_divided_by_14.
  rewrite n1_quotient_zero in c1_divided_by_14.
  rewrite (Z.add_0_r s1) in c1_divided_by_14.
  rewrite c1_divided_by_14 in c2_divided_by_14.
  rewrite c2_divided_by_14 in same_output.
  split.
  exact (Z.add_reg_l (s2 * 14) n1 n2 same_output).
  exact c2_divided_by_14.
Qed.

(** This lemma shows that the trumps composition is injective.  It is
a bit easier than in the minor composition case, because the suit is
always equal to 4. *)
Lemma trump_composition_is_injective :
  forall (n1 n2 s1 s2 : Z),
    is_valid_trump_number n1 ->
    is_trump_suit s1 ->
    is_valid_trump_number n2 ->
    is_trump_suit s2 ->
    (compose n1 s1 = compose n2 s2) ->
    (n1 = n2 /\ s1 = s2).
Proof.
  intros n1 n2 s1 s2 n1_trump s1_trump n2_trump s2_trump same_output.
  unfold compose in same_output.
  unfold is_trump_suit in s1_trump.
  unfold is_trump_suit in s2_trump.
  rewrite s1_trump in same_output.
  rewrite s2_trump in same_output.
  split.
  exact (Z.add_reg_l (4 * 14) n1 n2 same_output).
  rewrite s1_trump. rewrite s2_trump.
  trivial.
Qed.

(** The card composition is injective: if we can compose the same card
with 2 valid compositions, then these compositions are the same. *)
Lemma composition_is_injective :
  forall (n1 n2 s1 s2 : Z),
    is_valid_composition n1 s1 ->
    is_valid_composition n2 s2 ->
    (compose n1 s1 = compose n2 s2) ->
    (n1 = n2 /\ s1 = s2).
Proof.
  intros n1 n2 s1 s2 c1_valid c2_valid same_output.
  destruct (composition_is_injective_aux n1 n2 s1 s2 c1_valid c2_valid same_output) as [(n1_minor & s1_minor & n2_minor & s2_minor) | (n1_trump & s1_trump & n2_trump & s2_trump)].
  (** And now, both cases (all minor / all trumps). *)
  exact (minor_composition_is_injective n1 n2 s1 s2 n1_minor s1_minor n2_minor s2_minor same_output).
  exact (trump_composition_is_injective n1 n2 s1 s2 n1_trump s1_trump n2_trump s2_trump same_output).
Qed.

(** The minor decomposition algorithm returns a valid minor
decomposition, given a valid minor card. *)
Lemma minor_decomposition_works :
  forall (c : Z),
    is_valid_minor_card (c) ->
    (is_valid_minor_number (get_number c)
     /\ is_minor_suit (get_suit c)).
Proof.
  intros c c_minor.
  unfold is_valid_minor_card in c_minor.
  unfold get_number. unfold get_suit.
  case_eq (c <? 56)%Z. intros _.
  unfold is_valid_minor_number. unfold is_minor_suit.
  split.
  (** Prove that [c mod 14 < 14]. *)
  exact (Z.mod_pos_bound c 14%Z zero_lt_14).
  (** Prove that [c / 14 < 4], knowing [c < 56]. *)
  destruct c_minor as (c_pos, c_lt_56).
  split.
  exact (Z.div_le_mono 0 c 14 zero_lt_14 c_pos).
  exact (Z.div_lt_upper_bound c 14 4 zero_lt_14 c_lt_56).
  (** And now, the branch where c is a trump is contradictory. *)
  intros follow_false_branch.
  destruct c_minor as (_, c_lt_56).
  contradiction (cannot_follow_false_branch c c_lt_56 follow_false_branch).
Qed.

(** If you re-compose the result of a minor decomposition, you get the
same card. *)
Lemma minor_decomposition_reversible :
  forall (c : Z),
    is_valid_minor_card (c) ->
    compose (get_number c) (get_suit c) = c.
Proof.
  intros c (_, c_lt_56).
  unfold get_number. unfold get_suit. unfold compose.
  case_eq (c <? 56)%Z. intros _.
  (** Prove that [c / 14 * 14 + c mod 14 = c] *)
  rewrite (Z.mul_comm (c / 14) 14).
  exact (eq_sym (Z_div_mod_eq_full c 14)).
  (** Cannot follow the false branch. *)
  intros false_branch.
  contradiction (cannot_follow_false_branch c c_lt_56 false_branch).
Qed.

(** The trump decomposition algorithm returns a valid trump
decomposition, given a valid trump card. *)
Lemma trump_decomposition_works :
  forall (c : Z),
    is_valid_trump (c) ->
    (is_valid_trump_number (get_number c)
     /\ is_trump_suit (get_suit c)).
Proof.
  intros c c_trump.
  destruct c_trump as (c_ge_56, c_lt_77).
  unfold get_number. unfold get_suit. unfold is_valid_trump_number. unfold is_trump_suit.
  case_eq (c <? 56)%Z. intros follow_true_branch.
  contradiction (cannot_follow_true_branch c (Z.le_ge 56 c c_ge_56) follow_true_branch).
  intros _.
  (** So now we are following the “trump” branch. *)
  split. lia. reflexivity.
Qed.

(** If you re-compose the result of a trump decomposition, you get the
same card. *)
Lemma trump_decomposition_reversible :
  forall (c : Z),
    is_valid_trump (c) ->
    compose (get_number c) (get_suit c) = c.
Proof.
  intros c c_trump.
  destruct c_trump as (c_ge_56, _).
  unfold get_number. unfold get_suit. unfold compose.
  case_eq (c <? 56)%Z. intros follow_true_branch.
  contradiction (cannot_follow_true_branch c (Z.le_ge 56 c c_ge_56) follow_true_branch).
  intros _.
  (** So now we are following the “trump” branch. *)
  (** Prove that [4 * 14 + (c - 56) = c]. *)
  rewrite (Z.add_comm (4 * 14)%Z (c - 56)%Z).
  exact (Z.sub_simpl_r c 56).
Qed.

(** This lemma tells us that the domain of the decomposition is all
cards except the excuse. *)
Lemma decomposition_domain_simple :
  forall (c : Z),
    (c <> 77)%Z -> is_valid_card c -> (c < 77)%Z.
Proof.
  intros c c_neq_77 c_valid.
  unfold is_valid_card in c_valid.
  lia.
Qed.

(** Card composition is bijective and we know the inverse. *)
Theorem composition_is_bijective :
  forall (c n s : Z),
    (c <> 77)%Z ->
    is_valid_card c ->
    is_valid_composition n s ->
    compose n s = c <-> (n = get_number c /\ s = get_suit c).
Proof.
  intros c n s c_neq_77 c_valid_card compo_valid.
  pose (c_lt_77 := decomposition_domain_simple c c_neq_77 c_valid_card).
  destruct (can_split_at_56 c) as [c_ge_56 | c_lt_56].
  
  (** First case: [c >= 56]. *)
  cut (is_valid_trump c). intros c_valid_trump.
  split. intros composition_correct.
  (** We need to prove that [get_number c] / [get_suit c] is a valid
  composition. *)
  cut (is_valid_composition (get_number c) (get_suit c)). intros output_composable.
  cut (compose (get_number c) (get_suit c) = compose n s). intros output_recomposed.
  exact (composition_is_injective n (get_number c) s (get_suit c) compo_valid output_composable (eq_sym output_recomposed)).
  (** Prove that [compose (get_number c) (get_suit c) = compose n s]. *)
  rewrite (trump_decomposition_reversible c c_valid_trump).
  exact (eq_sym composition_correct).
  (** Prove that [(get_number c)] and [(get_suit c)] can be
  composed. *)
  destruct (trump_decomposition_works c c_valid_trump) as (gn_valid, gs_valid).
  unfold is_valid_composition. right. split. exact gn_valid. exact gs_valid.
  (** Prove the other way: [n = get_number c /\ s = get_suit c ->
  compose n s = c] *)
  intros (n_def, s_def).
  rewrite n_def. rewrite s_def.
  exact (trump_decomposition_reversible c c_valid_trump).
  (** Prove that [c] is a valid trump. *)
  unfold is_valid_trump.
  split. exact (Z.ge_le c 56 c_ge_56). exact c_lt_77.

  (** And now, the minor case. *)
  cut (is_valid_minor_card c). intros c_valid_minor.
  split. intros composition_correct.
  (** We need to prove that [get_number c] / [get_suit c] is a valid
  composition. *)
  cut (is_valid_composition (get_number c) (get_suit c)). intros output_composable.
  cut (compose (get_number c) (get_suit c) = compose n s). intros output_recomposed.
  exact (composition_is_injective n (get_number c) s (get_suit c) compo_valid output_composable (eq_sym output_recomposed)).
  (** Prove that [compose (get_number c) (get_suit c) = compose n s]. *)
  rewrite (minor_decomposition_reversible c c_valid_minor).
  exact (eq_sym composition_correct).
  (** Prove that [(get_number c)] and [(get_suit c)] can be
  composed. *)
  destruct (minor_decomposition_works c c_valid_minor) as (gn_valid, gs_valid).
  unfold is_valid_composition. left. split. exact gn_valid. exact gs_valid.
  (** Prove the other way: [n = get_number c /\ s = get_suit c ->
  compose n s = c] *)
  intros (n_def, s_def).
  rewrite n_def. rewrite s_def.
  exact (minor_decomposition_reversible c c_valid_minor).
  (** Prove that [c] is a valid minor card. *)
  unfold is_valid_minor_card.
  destruct c_valid_card as (c_pos, c_lt_78).
  split. exact c_pos. exact c_lt_56.
Qed.
