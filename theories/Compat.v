(** Compatibility with Frama-C. *)

Require Import Coq.ZArith.ZArith.
From Coq Require Import Lia.
Require Import Tarot.Cards.

Definition framac_is_valid_composition (n s : Z) :=
  (s = 4%Z) /\ (0 <= n <= 20%Z)%Z \/ (0 <= s <= 3%Z)%Z /\ (0 <= n <= 13%Z)%Z.

Theorem is_valid_composition_compat : forall (n s : Z),
    framac_is_valid_composition n s <-> Cards.is_valid_composition n s.
Proof.
  intros n s.
  unfold framac_is_valid_composition.
  unfold is_valid_composition.
  unfold is_valid_minor_number.
  unfold is_minor_suit.
  unfold is_valid_trump_number.
  unfold is_trump_suit.
  split. intros [(s_trump, (n_pos, n_le_20)) | ((s_pos, s_le_3), (n_pos, n_le_13))].
  right. split. split. exact n_pos. exact (Zle_lt_succ n 20 n_le_20). exact s_trump.
  left. split. split. exact n_pos. exact (Zle_lt_succ n 13 n_le_13).
  split. exact s_pos. exact (Zle_lt_succ s 3 s_le_3).
  intros [((n_pos, n_lt_14), (s_pos, s_lt_4)) | ((n_pos, n_lt_21), s_eq_4)].
  right. split. split. exact s_pos. exact (Zlt_succ_le s 3 s_lt_4).
  split. exact n_pos. exact (Zlt_succ_le n 13 n_lt_14).
  left. split. exact s_eq_4. split. exact n_pos. exact (Zlt_succ_le n 20 n_lt_21).
Qed.

Lemma follow_correct_branch_compat :
  forall (c : Z),
    (c <= 55%Z)%Z <-> (c <? 56)%Z = true.
Proof.
  intros c.
  destruct (Z.ltb_lt c 56) as (direct, indirect).
  split.
  intros c_le_55.
  exact (indirect (Zle_lt_succ c 55 c_le_55)).
  intros c_lt_56_true.
  exact (Zlt_succ_le c 55 (direct c_lt_56_true)).
Qed.  

Definition framac_p_suit (c s : Z) :=
  ((c <= 55%Z)%Z -> ((ZArith.BinInt.Z.quot c 14%Z) = s)) /\
    (~ (c <= 55%Z)%Z -> (s = 4%Z)).

Lemma fourteen_pos : (0 < 14)%Z.
Proof.
  lia.
Qed.

Theorem p_suit_compat : forall (c s : Z),
    (0 <= c)%Z ->
    framac_p_suit c s <-> s = Cards.get_suit c.
Proof.
  intros c s c_pos.
  destruct (follow_correct_branch_compat c) as (_, indirect).
  destruct (not_iff_compat (follow_correct_branch_compat c)) as (_, not_indirect).
  destruct (Bool.not_true_iff_false (c <? 56)%Z) as (_, convert_not).
  unfold Cards.get_suit.
  split.
  intros (minor_if_true, trump_if_not_true).
  case_eq (c <? 56)%Z. intros follow_true_branch.
  rewrite (eq_sym (minor_if_true (indirect follow_true_branch))).
  exact (Z.quot_div_nonneg c 14 c_pos fourteen_pos).
  intros follow_false_branch.
  exact (trump_if_not_true (not_indirect (convert_not follow_false_branch))).
  intros s_eq_tarot_suit.
  unfold framac_p_suit.
  case_eq (c <? 56)%Z.
  intros follow_true_branch.
  rewrite follow_true_branch in s_eq_tarot_suit.
  split. intros _.
  rewrite (Z.quot_div_nonneg c 14 c_pos fourteen_pos).
  exact (eq_sym s_eq_tarot_suit).
  intros not_follow_true_branch.
  contradiction (not_follow_true_branch (indirect follow_true_branch)).
  intros follow_false_branch.
  rewrite follow_false_branch in s_eq_tarot_suit.
  split.
  intros follow_true_branch.
  contradiction (not_indirect (convert_not follow_false_branch) follow_true_branch).
  intros _. exact s_eq_tarot_suit.
Qed.  

Definition framac_p_number (c n : Z) : Prop :=
  ((c <= 55%Z)%Z -> ((ZArith.BinInt.Z.rem c 14%Z) = n)) /\
    (~ (c <= 55%Z)%Z -> ((56%Z + n)%Z = c)).

Theorem p_number_compat : forall (c n : Z),
    (0 <= c)%Z ->
    framac_p_number c n <-> n = Cards.get_number c.
Proof.
  intros c n c_pos.
  destruct (follow_correct_branch_compat c) as (_, indirect).
  destruct (not_iff_compat (follow_correct_branch_compat c)) as (_, not_indirect).
  destruct (Bool.not_true_iff_false (c <? 56)%Z) as (_, convert_not).
  unfold framac_p_number. unfold get_number.
  split.
  intros (minor_if_true, trump_if_not_true).
  case_eq (c <? 56)%Z. intros follow_true_branch.
  rewrite (eq_sym (minor_if_true (indirect follow_true_branch))).
  exact (Z.rem_mod_nonneg c 14 c_pos fourteen_pos).
  intros follow_false_branch.
  rewrite (eq_sym (trump_if_not_true (not_indirect (convert_not follow_false_branch)))).
  rewrite (Z.add_comm 56 n).
  exact (eq_sym (Z.add_simpl_r n 56)).
  intros get_number_ok.
  case_eq (c <? 56)%Z. intros follow_true_branch.
  rewrite follow_true_branch in get_number_ok.
  rewrite (eq_sym (Z.rem_mod_nonneg c 14 c_pos fourteen_pos)) in get_number_ok.
  split.
  intros _.
  exact (eq_sym get_number_ok).
  intros not_follow_true_branch.
  contradiction (not_follow_true_branch (indirect follow_true_branch)).
  intros follow_false_branch.
  rewrite follow_false_branch in get_number_ok.
  split.
  intros follow_true_branch.
  contradiction (not_indirect (convert_not follow_false_branch) follow_true_branch).
  intros _.
  rewrite (Z.add_comm 56 n).
  rewrite get_number_ok.
  exact (Z.sub_simpl_r c 56).
Qed.

Definition framac_p_compose (n:Numbers.BinNums.Z) (s:Numbers.BinNums.Z)
    (c:Numbers.BinNums.Z) : Prop :=
  ((n + (14%Z * s)%Z)%Z = c).

Theorem p_compose_compat :
  forall (c n s : Z),
    framac_p_compose n s c <-> c = Cards.compose n s.
Proof.
  intros c n s.
  unfold framac_p_compose.
  unfold Cards.compose.
  rewrite (Z.add_comm n (14 * s)%Z).
  rewrite (Z.mul_comm 14 s).
  exact (Z.eq_sym_iff (s * 14 + n)%Z c).
Qed.

Theorem composition_is_bijective_compat :
  forall (c n s : Z),
    (0 <= c < 77)%Z ->
    framac_is_valid_composition n s ->
    framac_p_compose n s c <-> framac_p_suit c s /\ framac_p_number c n.
Proof.
  intros c n s (c_positive, c_lt_77) compo_valid_framac.
  destruct (is_valid_composition_compat n s) as (direct, _).
  pose (compo_valid := direct compo_valid_framac).
  cut (Cards.is_valid_card c). intros c_valid_tarot.
  cut (c <> 77)%Z. intros c_neq_77.
  pose (tarot_result := composition_is_bijective c n s c_neq_77 c_valid_tarot compo_valid).
  pose (compose_compat := p_compose_compat n s c).
  pose (number_compat := p_number_compat c n c_positive).
  pose (suit_compat := p_suit_compat c s c_positive).
  pose (reorder_and := and_comm (framac_p_suit c s) (framac_p_number c n)).
  pose (replace_suit := (iff_sym (and_iff_compat_l (framac_p_number c n) suit_compat))).
  pose (replace_number := (and_iff_compat_r (s = Cards.get_suit c) number_compat)).
  pose (rewrite_rhs :=
          (iff_sym
             (iff_trans reorder_and
                        (iff_trans (iff_sym replace_suit)
                                   (iff_trans replace_number
                                              (iff_sym tarot_result)))))).
  pose (rewrite_lhs := iff_trans (p_compose_compat c n s) (Z.eq_sym_iff c (Cards.compose n s))).
  exact (iff_trans rewrite_lhs rewrite_rhs).
  lia. unfold is_valid_card. lia.
Qed.
